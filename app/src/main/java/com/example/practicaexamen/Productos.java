package com.example.practicaexamen;

import java.io.Serializable;

public class Productos implements Serializable {
    private long  _ID;
    private String nombre;
    private String marca;
    private int precio;
    private String perecedero;
    private String noperecedero;


    public Productos(){

    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(String perecedero) {
        this.perecedero = perecedero;
    }

    public String getNoperecedero() {
        return noperecedero;
    }

    public void setNoperecedero(String noperecedero) {
        this.noperecedero = noperecedero;
    }
}
