package com.example.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Productos> productos= new ArrayList<Productos>();
    private EditText txtidd;
    private EditText texnombree;
    private EditText textmarcaa;
    private EditText textprecioo;
    private RadioButton Rperecederoo;
    private RadioButton Rnoperecederoo;
    private ActividadesProductos db;
    private int id = 1;
    private Productos savedContact = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         txtidd = (EditText) findViewById(R.id.textid);
         texnombree = (EditText) findViewById(R.id.texnombre);
         textmarcaa = (EditText) findViewById(R.id.textmarca);
         textprecioo = (EditText) findViewById(R.id.textprecio);

        final RadioButton Rperecedero = (RadioButton) findViewById(R.id.Rperecedero);
        final RadioButton Rnoperecedero = (RadioButton) findViewById(R.id.Rnoperecedero);

        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnIr = (Button) findViewById(R.id.btnIr);
        db = new ActividadesProductos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtidd.getText().toString().equals("")||texnombree.getText().toString().equals("")||
                        textmarcaa.getText().toString().equals("") || textprecioo.getText().toString().equals(""))
                { Toast.makeText(MainActivity.this, R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
                }

                Productos nProductos = new Productos();
                nProductos.setNombre(texnombree.getText().toString());
                nProductos.setMarca(textmarcaa.getText().toString());
                nProductos.setPrecio(0);
                nProductos.setPerecedero(Rperecedero.getText().toString());
                nProductos.setNoperecedero(Rnoperecedero.getText().toString());
                db.openDataBase();
                if (savedContact==null){
                    long idx = db.insertarProductos(nProductos);
                    Toast.makeText(MainActivity.this,"Se agregó contacto con ID: " +idx,Toast.LENGTH_SHORT).show();
                }

                else{
                    db.UpdateProductos(nProductos,id);
                    Toast.makeText(MainActivity.this,"Se actualizo el Registro: " +id,Toast.LENGTH_SHORT).show();
                }

                db.cerrar();

            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MainActivity.this,ProductoActivity.class);
                startActivity(intent);

            }
        });
    }

    public void limpiar()
    {
        txtidd.setText("");
        texnombree.setText("");
        textprecioo.setText("");
        textmarcaa.setText("");
        Rperecederoo.setChecked(false);
        Rnoperecederoo.setChecked(false);

    }
}
