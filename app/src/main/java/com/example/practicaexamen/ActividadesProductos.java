package com.example.practicaexamen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ActividadesProductos {
    private Context context;
    private sistema Sistema;
    private SQLiteDatabase db;

    private String[] columnsToRead = new String[]{
            DefinirTabla.Productos._ID,
            DefinirTabla.Productos.NOMBRE,
            DefinirTabla.Productos.MARCA,
            DefinirTabla.Productos.PRECIO,
            DefinirTabla.Productos.PERECEDERO,
            DefinirTabla.Productos.NOPERECEDERO
    };

    public ActividadesProductos(Context context) {
        this.context = context;
        Sistema = new sistema(this.context);
    }
    public void openDataBase() {
        db = Sistema.getWritableDatabase();
    }

    public long insertarProductos(Productos p) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Productos.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Productos.MARCA, p.getMarca());
        values.put(DefinirTabla.Productos.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Productos.PERECEDERO, p.getPerecedero());
        values.put(DefinirTabla.Productos.NOPERECEDERO, p.getNoperecedero());
        return db.insert(DefinirTabla.Productos.TABLE_NAME, null, values);
    }

    public long UpdateProductos(Productos p, long id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Productos.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Productos.MARCA, p.getMarca());
        values.put(DefinirTabla.Productos.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Productos.PERECEDERO, p.getPerecedero());
        values.put(DefinirTabla.Productos.NOPERECEDERO, p.getNoperecedero());
        String criterio = DefinirTabla.Productos._ID + " = " + id;
        return db.update(DefinirTabla.Productos.TABLE_NAME, values, criterio, null);
    }

    public int eliminarContacto(long id){
        String criterio = DefinirTabla.Productos._ID + " = " + id;
        return db.delete(DefinirTabla.Productos.TABLE_NAME, criterio, null);
    }

    public Productos readProductos(Cursor cursor){
        Productos p = new Productos();
        p.set_ID(cursor.getInt(0));
        p.setNombre(cursor.getString(1));
        p.setMarca(cursor.getString(2));
        p.setPrecio(Integer.parseInt(cursor.getString(3)));
        p.setPerecedero(cursor.getString(4));
        p.setNoperecedero(cursor.getString(5));
        return p;
    }

    public Productos getProductos(long id){
        Productos Productos = null;
        SQLiteDatabase db = Sistema.getWritableDatabase();
        Cursor c = db.query(DefinirTabla.Productos.TABLE_NAME,columnsToRead,DefinirTabla.Productos._ID + " = ? ",
                new String[] {String.valueOf(id)},null, null,null);
        if(c.moveToFirst())
            Productos = readProductos(c);
        c.close();
        return Productos;
    }

    public ArrayList<Productos> allProductos(){
        ArrayList<Productos> contactos = new ArrayList<Productos>();
        Cursor cursor = db.query(DefinirTabla.Productos.TABLE_NAME, null,null,null, null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Productos p = readProductos(cursor);
            contactos.add(p);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

    public void cerrar(){
        Sistema.close();
    }




}
