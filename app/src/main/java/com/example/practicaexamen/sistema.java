package com.example.practicaexamen;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class sistema extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_PRODUCTO =" CREATE TABLE " +
            DefinirTabla.Productos.TABLE_NAME + " ("+
            DefinirTabla.Productos._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Productos.NOMBRE+ TEXT_TYPE+ COMMA +
            DefinirTabla.Productos.MARCA+ TEXT_TYPE+ COMMA +
            DefinirTabla.Productos.PRECIO+ TEXT_TYPE+ COMMA +
            DefinirTabla.Productos.PERECEDERO+ TEXT_TYPE+ COMMA +
            DefinirTabla.Productos.NOPERECEDERO+ TEXT_TYPE+ COMMA + ")";

    private static final String SQL_DELETE_PRODUCTO ="DROP TABLE IF EXISTS "+
            DefinirTabla.Productos.TABLE_NAME;
    private static final int DATABASE_VERSION= 1;
    private static final String DATABASE_NAME = "agenda.db";


    public sistema(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PRODUCTO);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PRODUCTO);
        onCreate(db);
    }
}
